var express = require('express'),
    _ = require('lodash'),
    app = express(),
    cors = require('cors'),
    corsoptions = {
      origin: 'http://localhost:9000'
    },
    products = {};

initProducts();

app.use(cors(corsoptions));

app.get('/api/products', function(req, res) {
  res.send({
    products: _.values(products)
  });
});

app.get('/api/products/:sku', function(req, res) {
  var product = products[req.params.sku];
  if (product) {
    res.send(product);
  } else {
    res.status(404).end();
  }
});

var server = app.listen(3000, function() {
  var host = server.address().address;
  var port = server.address().port;

  console.log('Axxes Shop Backend listening at http://%s:%s', host, port);
});

function initProducts() {
  var productList = require('./products.json');

  _.each(productList, function (product) {
    console.log(product);
    var sku = _.chain(product.artist + '_' + product.albumTitle)
        .deburr()
        .snakeCase();
    products[sku] = _.assign(product, {sku: sku});
  });
}