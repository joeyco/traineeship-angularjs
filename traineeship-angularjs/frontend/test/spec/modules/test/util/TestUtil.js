(function () {

  'use strict';

  angular.module('com.axxes.angularjs.test').service('TestUtil', TestUtil);

  function TestUtil($q) {
    var self = this;
    self.getNoopFunction = angular.noop;
    self.getPromiseFunction = getPromiseFunction;

    function getPromiseFunction() {
      var defer = $q.defer(),
          promiseFunction = function () {
            return defer.promise;
          };
      promiseFunction.deferred = defer;

      return promiseFunction;
    }

  }

})();