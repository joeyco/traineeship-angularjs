(function() {
  'use strict';

  describe('asPrice', function() {
    var scope, createElement;

    beforeEach(module('com.axxes.angularjs.templates'));
    beforeEach(module('com.axxes.angularjs.common'));
    beforeEach(module('com.axxes.angularjs.test'));

    beforeEach(inject(function($injector) {
      var $compile = $injector.get('$compile'),
          $rootScope = $injector.get('$rootScope');

      scope = $rootScope.$new();

      createElement = function (price) {
        var tpl, elem;
        scope.price = price;

        tpl = '<as-price price="price"></as-price>';

        elem =  $compile(tpl)(scope);
        scope.$digest();
        return elem;
      };
    }));

    describe('When rendered', function () {
      it('should format the price', function () {
        var elem = createElement(10.34444444);

        expect(elem.find('span').text()).toEqual('€ 10.34');
      });
    });
  });
}());