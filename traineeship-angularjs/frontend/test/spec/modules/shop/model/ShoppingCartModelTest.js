(function() {
  'use strict';

  describe('ShoppingCartModel', function() {
    var ShoppingCartModel, ProductBuilder;

    beforeEach(module('com.axxes.angularjs.shop'));
    beforeEach(module('com.axxes.angularjs.test'));

    beforeEach(inject(function($injector) {
      ShoppingCartModel = $injector.get('ShoppingCartModel');
      ProductBuilder = $injector.get('ProductBuilder');
    }));

    describe('When initialized', function () {
      it('should expose correct properties and methods', function () {
        expect(ShoppingCartModel).toEqual(jasmine.objectContaining({
          products: [],
          total: 0,
          addProduct: jasmine.any(Function),
          removeProduct: jasmine.any(Function)
        }));
      });
    });

    describe('When addProduct is called', function () {
      describe('given the product isn\'t a duplicate', function () {
        it('should add the product to the basket', function () {
          var product = ProductBuilder.buildProduct();
          ShoppingCartModel.addProduct(product);

          expect(ShoppingCartModel.products[0]).toBe(product);
        });

        it('should calculate a correct total price', function () {
          ShoppingCartModel.addProduct(ProductBuilder.buildProduct('_first', 10));
          expect(ShoppingCartModel.total).toBe(10);

          ShoppingCartModel.addProduct(ProductBuilder.buildProduct('_first', 5.5));
          expect(ShoppingCartModel.total).toBe(15.5);
        });
      });

      describe('given the product is a duplicate', function () {
        it('shouldn\'t add the product to the basket', function () {
          var product = ProductBuilder.buildProduct();
          expect(ShoppingCartModel.addProduct(product)).toBeTruthy();
          expect(ShoppingCartModel.addProduct(product)).toBeFalsy();

          expect(ShoppingCartModel.products.length).toBe(1);
        });

        it('should calculate a correct total price', function () {
          var product = ProductBuilder.buildProduct();
          expect(ShoppingCartModel.addProduct(product)).toBeTruthy();
          expect(ShoppingCartModel.addProduct(product)).toBeFalsy();

          expect(ShoppingCartModel.total).toBe(10);
        });
      });
    });

    describe('When removeProduct is called', function () {
      var sku;

      beforeEach(function () {
        var product = ProductBuilder.buildProduct('1', 1);
        sku = product.sku;
        ShoppingCartModel.addProduct(product);
        ShoppingCartModel.addProduct(ProductBuilder.buildProduct('2', 2));
        ShoppingCartModel.addProduct(ProductBuilder.buildProduct('3', 3));
        ShoppingCartModel.addProduct(ProductBuilder.buildProduct('4', 4));
      });

      it('should remove the product', function () {
        ShoppingCartModel.removeProduct(sku);
        expect(ShoppingCartModel.products.length).toBe(3);
      });

      it('should calculate a correct total price', function () {
        ShoppingCartModel.removeProduct(sku);
        expect(ShoppingCartModel.total).toBe(9);
      });
    });
  });
}());