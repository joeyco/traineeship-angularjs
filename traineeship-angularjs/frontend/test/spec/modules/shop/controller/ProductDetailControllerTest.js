(function() {
  'use strict';

  describe('ProductDetailController', function() {
    var createController, ProductService, ShoppingCartModel, ProductBuilder, $rootScope;

    beforeEach(module('com.axxes.angularjs.shop', function($provide) {
      $provide.factory('ProductService', function($injector) {
        return $injector.get('ProductServiceMock');
      });
      $provide.factory('ShoppingCartModel', function($injector) {
        return $injector.get('ShoppingCartModelMock');
      });
    }));
    beforeEach(module('com.axxes.angularjs.test'));

    beforeEach(inject(function($injector) {
      var $controller = $injector.get('$controller');

      $rootScope = $injector.get('$rootScope');
      ProductService = $injector.get('ProductService');
      ShoppingCartModel = $injector.get('ShoppingCartModel');
      ProductBuilder = $injector.get('ProductBuilder');

      createController = function(params) {
        var scope = $rootScope.$new();
        return $controller('ProductDetailController', {
          $scope: scope,
          $routeParams: params
        });
      };
    }));

    describe('When initialized', function() {
      it('should expose correct methods and properties', function() {
        var vm = createController({sku: ProductBuilder.buildSku()});

        expect(vm).toEqual(jasmine.objectContaining({
          addToCart: jasmine.any(Function),
          model: jasmine.any(Object)
        }));
      });

      it('should call ProductService.retrieveProduct', function() {
        var sku = ProductBuilder.buildSku();
        spyOn(ProductService, 'retrieveProduct').and.callThrough();
        createController({sku: sku});
        expect(ProductService.retrieveProduct).toHaveBeenCalled();
      });

      describe('given teh ProductService.retrieveProduct promise is resolved', function() {
        it('should add the product to the model', function() {
          var product = ProductBuilder.buildProduct(),
              vm = createController({sku: product.sku});

          ProductService.retrieveProduct.deferred.resolve(product);
          $rootScope.$digest();

          expect(vm.model.product).toEqual(product);
        });
      });
    });

    describe('When addToCart is called', function () {
      it('should call ShoppingCartModel.addProduct', function () {
        var product = ProductBuilder.buildProduct(),
            vm = createController({sku: product.sku});

        vm.model.product = product;

        spyOn(ShoppingCartModel, 'addProduct');
        vm.addToCart();
        expect(ShoppingCartModel.addProduct).toHaveBeenCalledWith(product);

      });
    });
  });
}());