(function () {
  'use strict';

  angular.module('com.axxes.angularjs.test').service('ProductBuilder', Builder);

  function Builder(_) {
    var self = this;

    self.buildProduct = buildProduct;
    self.buildProducts = buildProducts;
    self.buildSku = buildSku;

    function buildProduct(suffix, price) {
      suffix = '_' + suffix || '';
      return {
        sku: buildSku(),
        artist: 'Artist' + suffix,
        albumTitle: 'Album_Title' + suffix,
        albumArt: 'https://upload.wikimedia.org/wikipedia/en/thumb/7/78/Book_of_Souls_Iron_Maiden.jpg/440px-Book_of_Souls_Iron_Maiden.jpg',
        price: price || 10,
        genre: 'Metal',
        tracks: [
          {name: 'track1', durantion: '1:00'},
          {name: 'track2', durantion: '2:00'}
        ]
      };
    }

    function buildProducts(n) {
      var r = [];
      _.times(n || 2, function (i) {
        r.push(buildProduct(i));
      });

      return r;
    }

    function buildSku() {
      return _.uniqueId('sku_');
    }
  }
}());