(function () {
  'use strict';

  angular.module('com.axxes.angularjs.test').service('ProductGatewayMock', Mock);

  function Mock(TestUtil) {
    var self = this;
    self.retrieveAllProducts = TestUtil.getPromiseFunction();
    self.retrieveProduct = TestUtil.getPromiseFunction();
  }
}());