(function () {
  'use strict';

  angular.module('com.axxes.angularjs.test').service('ShoppingCartModelMock', Mock);

  function Mock() {
    var self = this;

    self.products = [];
    self.total = 0;

    self.addProduct = angular.noop;
    self.removeProduct = angular.noop;
  }
}());