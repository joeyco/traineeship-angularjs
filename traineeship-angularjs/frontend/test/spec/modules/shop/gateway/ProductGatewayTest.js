(function() {
  'use strict';

  describe('ProductGatewayTest', function() {
    var ProductGateway, ProductBuilder, $httpBackend, BACKEND_URL;

    beforeEach(module('com.axxes.angularjs.shop'));
    beforeEach(module('com.axxes.angularjs.test'));

    beforeEach(inject(function($injector) {
      ProductGateway = $injector.get('ProductGateway');
      ProductBuilder = $injector.get('ProductBuilder');
      $httpBackend = $injector.get('$httpBackend');
      BACKEND_URL = $injector.get('BACKEND_URL');
    }));

    describe('When initialized', function() {
      it('should expose correct properties and methods', function() {
        expect(ProductGateway).toEqual(jasmine.objectContaining({
          retrieveAllProducts: jasmine.any(Function),
          retrieveProduct: jasmine.any(Function)
        }));
      });
    });

    describe('When retrieveAllProducts is called', function() {
      var allProductsRequestHandler;
      beforeEach(function() {
        allProductsRequestHandler = $httpBackend.when('GET', BACKEND_URL + '/products');
      });

      describe('given the call to the backend was successful', function(done) {
        it('it should resolve the promise with the correct products', function() {
          var expectedProducts = ProductBuilder.buildProducts(),
              promise;

          allProductsRequestHandler.respond({products: expectedProducts});
          $httpBackend.expectGET(BACKEND_URL + '/products');
          promise = ProductGateway.retrieveAllProducts();
          $httpBackend.flush();

          promise.then(function(products) {
            expect(products).toEqual(expectedProducts);
          }).finally(done);
        });
      });

      describe('given the call to the backend was unsuccessful', function(done) {
        it('it should reject the promise', function() {
          var promise;

          allProductsRequestHandler.respond(404, 'Not found');
          $httpBackend.expectGET(BACKEND_URL + '/products');
          promise = ProductGateway.retrieveAllProducts();
          $httpBackend.flush();

          promise.catch(function(result) {
            expect(result).toEqual({
              status: 404,
              data: 'Not found'
            });
          }).finally(done);
        });
      });
    });

    describe('When retrieveProduct is called', function() {

      var productRequestHandler, sku;

      beforeEach(function() {
        sku = ProductBuilder.buildSku();
        productRequestHandler = $httpBackend.when('GET', BACKEND_URL + '/products/' + sku);
      });

      describe('given the call to the backend was successful', function(done) {
        it('it should resolve the promise with the correct product', function() {
          var expectedProduct = ProductBuilder.buildProduct(),
              promise;

          productRequestHandler.respond(expectedProduct);
          $httpBackend.expectGET(BACKEND_URL + '/products/' + sku);
          promise = ProductGateway.retrieveProduct(sku);
          $httpBackend.flush();

          promise.then(function(product) {
            expect(product).toEqual(expectedProduct);
          }).finally(done);
        });
      });

      describe('given the call to the backend was unsuccessful', function(done) {
        it('it should reject the promise', function() {
          var promise;

          productRequestHandler.respond(404, 'Not found');
          $httpBackend.expectGET(BACKEND_URL + '/products/' + sku);
          promise = ProductGateway.retrieveProduct(sku);
          $httpBackend.flush();

          promise.catch(function(result) {
            expect(result).toEqual({
              status: 404,
              data: 'Not found'
            });
          }).finally(done);
        });
      });
    });

    afterEach(function() {
      $httpBackend.verifyNoOutstandingExpectation();
      $httpBackend.verifyNoOutstandingRequest();
    });
  });
}());