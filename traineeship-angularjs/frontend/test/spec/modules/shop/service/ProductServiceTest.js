(function() {
  'use strict';

  describe('ProductService', function() {
    var ProductService, ProductGateway, TestUtil, ProductBuilder;

    beforeEach(module('com.axxes.angularjs.shop', function($provide) {
      $provide.factory('ProductGateway', function($injector) {
        return $injector.get('ProductGatewayMock');
      });
    }));
    beforeEach(module('com.axxes.angularjs.test'));

    beforeEach(inject(function($injector) {
      TestUtil = $injector.get('TestUtil');
      ProductBuilder = $injector.get('ProductBuilder');
      ProductGateway = $injector.get('ProductGateway');
      ProductService = $injector.get('ProductService');
    }));

    describe('When initialized', function() {
      it('should expose the correct methods', function() {
        expect(ProductService).toEqual(jasmine.objectContaining({
          retrieveAllProducts: jasmine.any(Function),
          retrieveProduct: jasmine.any(Function)
        }));
      });
    });

    describe('When retrieveAllProducts is called', function () {
      it('should call ProductGateway.retrieveAllProducts()', function () {
        spyOn(ProductGateway, 'retrieveAllProducts');
        ProductService.retrieveAllProducts();
        expect(ProductGateway.retrieveAllProducts).toHaveBeenCalled();
      });
    });

    describe('When retrieveProduct is called', function () {
      it('should call ProductGateway.retrieveProduct()', function () {
        var sku = ProductBuilder.buildSku();
        spyOn(ProductGateway, 'retrieveProduct');
        ProductService.retrieveProduct(sku);
        expect(ProductGateway.retrieveProduct).toHaveBeenCalledWith(sku);
      });
    });

  });

}());