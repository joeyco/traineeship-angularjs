(function() {
  'use strict';

  describe('asShoppingCart', function() {
    var scope, createController, ShoppingCartModel, ProductBuilder;

    beforeEach(module('com.axxes.angularjs.templates'));
    beforeEach(module('com.axxes.angularjs.shop', function($provide) {
      $provide.factory('ShoppingCartModel', function($injector) {
        return $injector.get('ShoppingCartModelMock');
      });
    }));
    beforeEach(module('com.axxes.angularjs.test'));

    beforeEach(inject(function($injector) {
      var $compile = $injector.get('$compile'),
          $rootScope = $injector.get('$rootScope');

      ShoppingCartModel = $injector.get('ShoppingCartModel');
      ProductBuilder = $injector.get('ProductBuilder');

      scope = $rootScope.$new();

      createController = function() {
        var tpl, elem;

        tpl = '<as-shopping-cart></as-shopping-cart>';
        elem = $compile(tpl)(scope);

        scope.$digest();
        return elem.controller('asShoppingCart');
      };
    }));

    describe('When initialized', function () {
      it('should expose correct methods and properties', function() {
        var vm = createController();
        expect(vm).toEqual(jasmine.objectContaining({
          removeProduct: jasmine.any(Function),
          model: ShoppingCartModel
        }));
      });
    });

    describe('When removeProduct is called', function () {
      it('should call ShoppingCartModel.removeProduct', function () {
        var vm = createController(),
            product = ProductBuilder.buildProduct();

        spyOn(ShoppingCartModel, 'removeProduct');
        vm.removeProduct(product);
        expect(ShoppingCartModel.removeProduct).toHaveBeenCalledWith(product.sku);
      });
    });

  });
}());