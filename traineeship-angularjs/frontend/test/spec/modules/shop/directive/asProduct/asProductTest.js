(function() {
  'use strict';

  describe('asProduct', function() {
    var scope, createController, ShoppingCartModel, ProductBuilder;

    beforeEach(module('com.axxes.angularjs.templates'));
    beforeEach(module('com.axxes.angularjs.shop', function($provide) {
      $provide.factory('ShoppingCartModel', function($injector) {
        return $injector.get('ShoppingCartModelMock');
      });
    }));
    beforeEach(module('com.axxes.angularjs.test'));

    beforeEach(inject(function($injector) {
      var $compile = $injector.get('$compile'),
          $rootScope = $injector.get('$rootScope');

      ShoppingCartModel = $injector.get('ShoppingCartModel');
      ProductBuilder = $injector.get('ProductBuilder');

      scope = $rootScope.$new();

      createController = function(product) {
        var tpl, elem;

        scope.product = product;

        tpl = '<as-product product="product"></as-product>';
        elem = $compile(tpl)(scope);

        scope.$digest();
        return elem.controller('asProduct');
      };
    }));

    describe('When initialed', function() {
      it('should expose correct methods and properties', function() {
        var vm = createController(ProductBuilder.buildProduct());
        expect(vm).toEqual(jasmine.objectContaining({
          addToCart: jasmine.any(Function)
        }));
      });
    });

    describe('When addToCart is called', function() {
      it('should call ShoppingCartModel.addProduct', function() {
        var product = ProductBuilder.buildProduct(),
            vm = createController(product);

        spyOn(ShoppingCartModel, 'addProduct');
        vm.addToCart();
        expect(ShoppingCartModel.addProduct).toHaveBeenCalledWith(product);
      });
    });

  });
}());