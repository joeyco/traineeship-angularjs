(function() {
  'use strict';

  describe('asProductList', function() {
    var scope, createController, ProductService, ProductBuilder;

    beforeEach(module('com.axxes.angularjs.templates'));
    beforeEach(module('com.axxes.angularjs.shop', function($provide) {
      $provide.factory('ProductService', function($injector) {
        return $injector.get('ProductServiceMock');
      });
    }));
    beforeEach(module('com.axxes.angularjs.test'));

    beforeEach(inject(function($injector) {
      var $compile = $injector.get('$compile'),
          $rootScope = $injector.get('$rootScope');

      ProductService = $injector.get('ProductService');
      ProductBuilder = $injector.get('ProductBuilder');

      scope = $rootScope.$new();

      createController = function() {
        var tpl, elem;

        tpl = '<as-product-list></as-product-list>';
        elem = $compile(tpl)(scope);

        scope.$digest();
        return elem.controller('asProductList');
      };
    }));

    describe('When initialed', function () {
      it('should call ProductService.retrieveAllProducts', function() {
        spyOn(ProductService, 'retrieveAllProducts').and.callThrough();
        createController();
        expect(ProductService.retrieveAllProducts).toHaveBeenCalled();
      });

      describe('given the retrieveAllProducts promise is resolved', function () {
        it('should add the products to the model', function () {
          var expectedProducts = ProductBuilder.buildProducts(),
              vm = createController();
          ProductService.retrieveAllProducts.deferred.resolve(expectedProducts);
          scope.$digest();
          expect(vm.model.products).toEqual(expectedProducts);
        });
      });
    });

  });
}());