(function() {
  'use strict';

  var gulp = require('gulp'),
      concat = require('gulp-concat'),
      install = require('gulp-install'),
      del = require('del'),
      connect = require('gulp-connect'),
      unCache = require('gulp-uncache'),
      sass = require('gulp-sass'),
      open = require('gulp-open'),
      sourcemaps = require('gulp-sourcemaps'),
      templateCache = require('gulp-angular-templatecache'),
      jf = require('jsonfile'),
      runSequence = require('run-sequence'),
      os = require('os'),
      karma = require('gulp-karma'),
      jshint = require('gulp-jshint'),
      jscs = require('gulp-jscs'),
      jscs_stylish = require('gulp-jscs-stylish'),
      dependencies = jf.readFileSync('dependencies.json'),
      DIST_DIR = './dist',
      SOURCE = 'src/app/**/*.js',
      TEST_SOURCE = 'test/**/*.js',
      TEMPLATES = 'src/app/**/*.html',
      MAIN_MODULE = 'com.axxes.angularjs',
      SERVE_PORT = 9000;

  dependencies.test = dependencies.javascript.concat(dependencies.test.concat([SOURCE,'test/spec/modules/test/**/*.js' ,TEST_SOURCE, TEMPLATES]));

  gulp.task('copy-html', function() {
    return gulp.src('./src/public/index.html')
        .pipe(unCache({
          append: 'time'
        }))
        .pipe(gulp.dest(DIST_DIR));
  });

  gulp.task('compile-templates', function() {
    return gulp.src(TEMPLATES)
        .pipe(templateCache({
          module: MAIN_MODULE,
          root: ''
        }))
        .pipe(concat('templates.js'))
        .pipe(gulp.dest(DIST_DIR));
  });

  gulp.task('build-javascript', ['compile-templates'], function() {
    return gulp.src(dependencies.javascript.concat([SOURCE, DIST_DIR + '/templates.js']))
        .pipe(sourcemaps.init())
        .pipe(concat('app.js'))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(DIST_DIR));
  });

  gulp.task('build-scss', function() {
    return gulp.src(dependencies.css.concat(['src/public/sass/**/*.scss']))
        .pipe(sass().on('error', sass.logError))
        .pipe(concat('main.css'))
        .pipe(gulp.dest(DIST_DIR + '/css'));
  });

  gulp.task('copy-fonts', function() {
    return gulp.src(dependencies.fonts)
        .pipe(gulp.dest(DIST_DIR + '/fonts'));
  });

  gulp.task('build-css', ['build-scss', 'copy-fonts']);

  gulp.task('watch', function() {
    gulp.watch([SOURCE, TEMPLATES], ['watch-javascript']);
    gulp.watch('src/public/index.html', ['watch-index-html']);
    gulp.watch('src/public/sass/**/*.scss', ['watch-css']);
  });

  gulp.task('watch-javascript', function(done) {
    runSequence('build-javascript', 'reload', done);
  });

  gulp.task('watch-css', function(done) {
    runSequence('build-css', 'reload', done);
  });

  gulp.task('watch-index-html', function(done) {
    runSequence('copy-html', 'reload', done);
  });

  gulp.task('clean', function(done) {
    del([DIST_DIR], done);
  });

  gulp.task('reload', function() {
    return gulp.src(DIST_DIR)
        .pipe(connect.reload());
  });

  gulp.task('open-browser', function() {
    var options, browser;

    browser = os.platform() === 'linux' ? 'google-chrome' : (
        os.platform() === 'darwin' ? 'google chrome' : (
            os.platform() === 'win32' ? 'chrome' : 'firefox'));
    options = {
      uri: 'http://localhost:' + SERVE_PORT,
      app: browser
    };
    return gulp.src('./dist/index.html')
        .pipe(open(options));
  });

  gulp.task('start-server', function() {
    connect.server({
      livereload: true,
      root: [DIST_DIR],
      port: SERVE_PORT,
      host: 'localhost'
    });
  });

  gulp.task('build', ['build-javascript', 'build-css']);

  gulp.task('serve', function() {
    runSequence('clean', 'build', 'copy-html', 'start-server',
        function(err) {
          if (!err) {
            gulp.start('watch');
            gulp.start('open-browser');
          }
        });
  });

  gulp.task('test', function(done) {
    gulp.src(dependencies.test)
        .pipe(karma({
          configFile: 'karma.conf.js',
          action: 'run'
        }))
        .on('end', done);
  });

  /*gulp.task('jshint', function () {
    return gulp.src([SOURCE, TEST_SOURCE])
        .pipe(jshint())
        .pipe(jshint.reporter('jshint-stylish'))
        .pipe(jscs());
  });

  gulp.task('jscs', function () {
    return gulp.src([SOURCE, TEST_SOURCE])

  })*/

  gulp.task('check', function () {
    return gulp.src([SOURCE, TEST_SOURCE])
        .pipe(jshint())
        .pipe(jscs())
        .on('error', function () {})
        .pipe(jscs_stylish.combineWithHintResults())
        .pipe(jshint.reporter('jshint-stylish'));
  });

  gulp.task('default', ['serve']);
}());
