(function() {
  'use strict';

  angular.module('com.axxes.angularjs.shop').service('ProductGateway', Implementation);

  function Implementation($q, $http, BACKEND_URL) {

    var self = this;

    self.retrieveAllProducts = retrieveAllProducts;
    self.retrieveProduct = retrieveProduct;

    function retrieveAllProducts() {
      var deferred = $q.defer();

      $http.get(BACKEND_URL + '/products')
          .success(function(result) {
            deferred.resolve(result.products);
          })
          .error(function(data, status) {
            deferred.reject({status: status, data: data});
          });

      return deferred.promise;
    }

    function retrieveProduct(sku) {
      var deferred = $q.defer();

      $http.get(BACKEND_URL + '/products/' + sku)
          .success(function(result) {
            deferred.resolve(result);
          })
          .error(function(data, status) {
            deferred.reject({status: status, data: data});
          });

      return deferred.promise;
    }

  }
}());