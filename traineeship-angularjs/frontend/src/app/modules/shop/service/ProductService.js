(function () {
  'use strict';

  angular.module('com.axxes.angularjs.shop').service('ProductService', Implementation);

  function Implementation(ProductGateway) {

    var self = this;

    self.retrieveAllProducts = retrieveAllProducts;
    self.retrieveProduct = retrieveProduct;

    function retrieveAllProducts() {
      return ProductGateway.retrieveAllProducts();
    }

    function retrieveProduct(sku) {
      return ProductGateway.retrieveProduct(sku);
    }

  }
}());