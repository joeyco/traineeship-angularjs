(function () {
  'use strict';

  angular.module('com.axxes.angularjs.shop').directive('asShoppingCart', Implementation);

  function Implementation() {

    return {
      templateUrl: 'modules/shop/directive/asShoppingCart/asShoppingCart.html',
      scope: {},
      restrict: 'E',
      controllerAs: 'shoppingCartVm',
      bindToController: true,
      controller: Controller
    };

    function Controller(ShoppingCartModel) {
      var vm = this;
      vm.removeProduct = removeProduct;

      initModel();

      function initModel() {
        vm.model = ShoppingCartModel;
      }

      function removeProduct(product) {
        ShoppingCartModel.removeProduct(product.sku);
      }
    }

  }
}());