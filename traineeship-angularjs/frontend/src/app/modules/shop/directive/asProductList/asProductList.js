(function () {
  'use strict';

  angular.module('com.axxes.angularjs.shop').directive('asProductList', Implementation);

  function Implementation() {

    return {
      templateUrl: 'modules/shop/directive/asProductList/asProductList.html',
      scope: {},
      restrict: 'E',
      controllerAs: 'productListVm',
      bindToController: true,
      controller: Controller
    };

    function Controller(ProductService) {
      var vm = this;

      initModel();

      function initModel() {
        vm.model = {};

        ProductService.retrieveAllProducts().then(function (products) {
          vm.model.products = products;
        });
      }
    }

  }
}());