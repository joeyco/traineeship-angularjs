(function () {
  'use strict';

  angular.module('com.axxes.angularjs.shop').directive('asProduct', Implementation);

  function Implementation() {

    return {
      templateUrl: 'modules/shop/directive/asProduct/asProduct.html',
      scope: {},
      restrict: 'E',
      controllerAs: 'productVm',
      bindToController: {
        product: '='
      },
      controller: Controller
    };

    function Controller(ShoppingCartModel) {
      var vm = this;
      vm.addToCart = addToCart;

      function addToCart() {
        ShoppingCartModel.addProduct(vm.product);
      }
    }

  }
}());