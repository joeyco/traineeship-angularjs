(function () {
  'use strict';

  angular.module('com.axxes.angularjs.shop').controller('ProductDetailController', Controller);

  function Controller(ProductService, ShoppingCartModel, $routeParams) {

    var vm = this;

    vm.addToCart = addToCart;

    initModel();

    function initModel() {
      vm.model = {

      };

      ProductService.retrieveProduct($routeParams.sku).then(function (product) {
        vm.model.product = product;
      });
    }

    function addToCart() {
      ShoppingCartModel.addProduct(vm.model.product);
    }

  }
}());