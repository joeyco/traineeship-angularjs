(function () {
  'use strict';

  angular.module('com.axxes.angularjs.shop').service('ShoppingCartModel', Implementation);

  function Implementation(_) {

    var self = this;

    self.products = [];
    self.total = 0;

    self.addProduct = addProduct;
    self.removeProduct = removeProduct;

    function addProduct(product) {
      var isDuplicateProduct = _.includes(self.products, product);
      if (!isDuplicateProduct) {
        self.products.push(product);
        recalculateTotal();
      }
      return !isDuplicateProduct;
    }

    function removeProduct(sku) {
      _.remove(self.products, function (product) {
        return product.sku === sku;
      });
      recalculateTotal();
    }

    function recalculateTotal() {
      self.total = _.chain(self.products)
          .map(mapPrice)
          .reduce(reduceTotal)
          .value();
    }

    function mapPrice(product) {
      return product.price;
    }

    function reduceTotal(total, price) {
      return total + price;
    }

  }
}());