(function () {
  'use strict';

  angular.module('com.axxes.angularjs.shop', ['ngRoute', 'com.axxes.angularjs.common']).config(['$routeProvider',
    function($routeProvider) {
      $routeProvider.
          when('/shop', {
            templateUrl: 'modules/shop/view/index.html'
          }).
          when('/product/:sku', {
            templateUrl: 'modules/shop/view/productDetail.html'
          }).
          otherwise({
            redirectTo: '/shop'
          });
    }]);
}());