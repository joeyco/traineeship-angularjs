(function () {
  'use strict';

  angular.module('com.axxes.angularjs.common').directive('asPrice', Implementation);

  function Implementation() {

    return {
      templateUrl: 'modules/common/directive/asPrice.html',
      scope: {},
      restrict: 'E',
      controllerAs: 'priceVm',
      bindToController: {
        price: '='
      },
      controller: angular.noop
    };

  }
}());