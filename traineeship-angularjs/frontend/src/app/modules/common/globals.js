(function () {
  'use strict';

  angular.module('com.axxes.angularjs.common')
      .constant('_', _)
      .constant('BACKEND_URL', 'http://localhost:3000/api');
}());